FROM node:7

# Make directories to store the dependencies and the application code:

RUN mkdir -p /package
RUN mkdir -p /application
WORKDIR /application

# Set up the basics:

ENV PORT 8080
EXPOSE 8080

# Tell node where to find dependencies (they are not installed in the
# normal location

ENV NODE_PATH /package/node_modules
ENV PATH /package/node_modules/.bin:$PATH

# Make incremental updates to the dependencies:

COPY yarn.lock package.json /application/
RUN yarn install --modules-folder=$NODE_PATH

# By default, run the application with node:

CMD [ "node","index.js" ]
